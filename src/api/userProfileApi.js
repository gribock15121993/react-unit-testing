export const getUserByIdAPI = (id = 1) => fetch(`https://jsonplaceholder.typicode.com/users/${id}`)
    .then(response => {
        if (response.ok) {
            return response.json();
        }
        throw new Error(`Ошибка, код ${response.status}`);
    })
