import { render, screen } from '@testing-library/react';
import ButtonWithLoader from "./ButtonWithLoader";

describe('ButtonWithLoader component', () => {

    it('display own title prop', () => {
        const TEST_TITLE = 'TEST_TITLE';
        render(<ButtonWithLoader
            title={TEST_TITLE}
        />);

        const componentNode = screen.getByText(TEST_TITLE);
        expect(componentNode).toBeInTheDocument();
    });

    it('display own loadingTitle prop', () => {
        const TEST_LOADING_TITLE = 'LOADING';
        render(<ButtonWithLoader
            isLoading
            loadingTitle={TEST_LOADING_TITLE}
        />);

        const componentNode = screen.getByText(TEST_LOADING_TITLE);

        expect(componentNode).toBeInTheDocument();
    });

    it('not display own loadingTitle prop, but display own title', () => {
        const TEST_TITLE = 'TEST_TITLE';
        const TEST_LOADING_TITLE = 'LOADING';
        render(<ButtonWithLoader
            loadingTitle={TEST_LOADING_TITLE}
            title={TEST_TITLE}
        />);

        const notExistedButton = screen.queryByText(TEST_LOADING_TITLE);
        const existedButton = screen.queryByText(TEST_TITLE)

        expect(existedButton).toBeInTheDocument();
        expect(notExistedButton).not.toBeInTheDocument();
    });

    it('disabled when isLoading = true', () => {
        const TEST_LOADING_TITLE = 'LOADING';
        render(<ButtonWithLoader
            loadingTitle={TEST_LOADING_TITLE}
            isLoading
        />);

        const existedButton = screen.queryByText(TEST_LOADING_TITLE)

        expect(existedButton).toBeDisabled();
    });
})
