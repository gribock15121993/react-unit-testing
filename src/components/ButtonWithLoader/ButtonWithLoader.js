import React from 'react';
import clsx from 'clsx';

const ButtonWithLoader = ({ isLoading, title, loadingTitle, className, ...props }) => {
    return <button
        className={clsx(`bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded ${className}`,
            isLoading && 'opacity-50 cursor-not-allowed')}
        disabled={isLoading}
        {...props}
    >
        {isLoading ? loadingTitle : title}
    </button>
}

export default ButtonWithLoader;

// 1) Отображается необходимый title
// 2) Отображается переданный loadingTitle
// 3) При isLoading = true onClick не срабатывает (кнопка disabled)
