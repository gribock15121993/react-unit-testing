import React from 'react';
import { FetchMock } from '@react-mock/fetch';
import { renderHook, act } from '@testing-library/react-hooks';
import {useUserProfile} from "./useUserProfile";

describe('useUserProfile hook', () => {
    it('change isLoading when call promise', async () => {
        const wrapper = ({ children }) => <FetchMock>{children}</FetchMock>
        const { result, waitForNextUpdate } = renderHook(() => useUserProfile(), { wrapper })
        expect(result.current.isLoading).toBe(false);
        act(() => {
            result.current.getUserProfile()
        })
        expect(result.current.isLoading).toBe(true);
        await waitForNextUpdate();
        expect(result.current.isLoading).toBe(false);
    });

    it('return error if promise was rejected', async () => {
        const wrapper = ({ children }) => {
            return <FetchMock
                options={{
                    matcher: 'https://jsonplaceholder.typicode.com/users/1',
                    response: 404,
                    method: 'GET',
                }}
            >{children}</FetchMock>
        }
        const { result, waitForNextUpdate } = renderHook(() => useUserProfile(), { wrapper })
        act(() => {
            result.current.getUserProfile()
        })
        await waitForNextUpdate();
        expect(result.current.error.message).toBe('Ошибка, код 404')
        expect(result.current.isLoading).toBe(false)
    });

    it('return user if promise was resolved', async () => {
        const mockUser = { name: 'John' }
        const wrapper = ({ children }) => {
            return <FetchMock
                options={{
                    matcher: 'https://jsonplaceholder.typicode.com/users/1',
                    response: mockUser,
                    method: 'GET',
                }}
            >{children}</FetchMock>
        }
        const { result, waitForNextUpdate } = renderHook(() => useUserProfile(), { wrapper })
        act(() => {
            result.current.getUserProfile()
        })
        await waitForNextUpdate();
        expect(result.current.userProfileData).toEqual(mockUser)
    });
})
