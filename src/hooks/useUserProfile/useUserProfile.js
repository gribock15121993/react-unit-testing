import {useState} from "react";
import {getUserByIdAPI} from "../../api/userProfileApi";

export const useUserProfile = (userId = 1) => {
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState(null);

    const [userProfileData, setUserProfileData] = useState(null);

    const getUserProfile = async () => {
        setError(null);
        setIsLoading(true);
        try {
            const data = await getUserByIdAPI(userId);
            setUserProfileData(data);
        } catch (e) {
            setError(e)
        }
        setIsLoading(false);
    }

    return {
        isLoading,
        error,
        userProfileData,
        getUserProfile,
    }
}
